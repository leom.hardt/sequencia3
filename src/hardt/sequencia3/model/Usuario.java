/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hardt.sequencia3.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aluno
 */
public class Usuario {
    int id;
    String email;
    String senha;
    List<Interesse> interesses;
    
    

    public Usuario(int id, String nome, String senha) {
        this.id = id;
        this.email = nome;
        this.senha = senha;
        this.interesses = new ArrayList<>();
    }

    public String getEmail() {
        return email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public List<Interesse> getInteresses() {
        return interesses;
    }
    
    public String getInteressesString() {
        String s = "";
        for(Interesse interesse : interesses){
            s += interesse + ",";
        }
        return s;
    }
    
    
    public void setEmail(String email) {
        this.email = email;
    }    
    

    public void setInteresses(List<Interesse> interesses) {
        this.interesses = interesses;
    }
    
    
    @Override
    public String toString(){
        return "User:" + email + "; PWD: " + senha;
    }
    
}
