
package hardt.sequencia3.model;

/**
 *
 * @author Léo H.
 */
public class Interesse {
    private String nome;
    private int id;

    public Interesse(int id, String nome) {
        this.nome = nome;
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
}
