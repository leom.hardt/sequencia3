/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hardt.sequencia3.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aluno
 */
public class UsuarioDB {
    /**
     * Useful method for not copying code. In the end, all connections are equal.
     */
    private static Connection getConnection() throws SQLException{
        return DriverManager.getConnection("jdbc:oracle:thin:@oracle.canoas.ifrs.edu.br:1521:XE",
                Dados.getUsername(), Dados.getPassword());   
    }
    
    /**
     * Creates the tables in the database.
     * @return true if created.
     */
    public static boolean createTable() throws Exception{
        
        
        boolean created = false;
        Class.forName("oracle.jdbc.driver.OracleDriver");  
        final String CREATE_TABLE_USER = "CREATE TABLE userx(" 
                + "  id integer constraint pkuser primary key,"
                + "  username varchar2(40)," 
                + "  password varchar2(40)" 
                + ")";
        
        final String CREATE_TABLE_INTEREST = "CREATE TABLE interestx(" 
                + "  id integer constraint pkinterest primary key,"
                + "  value varchar2(40)," 
                + "  iduser integer," 
                + "  CONSTRAINT fk_iduser FOREIGN KEY (iduser) REFERENCES userX(id)" 
                + ")";
        final String CREATE_SEQUENCE_USER = "CREATE SEQUENCE user_seq";
        final String CREATE_SEQUENCE_INTEREST = "CREATE SEQUENCE interest_seq";
        try(Connection c = getConnection()){
            boolean createdSequenceUser = c.prepareStatement(CREATE_SEQUENCE_USER).execute();
            boolean createdSequenceInterest = c.prepareStatement(CREATE_SEQUENCE_INTEREST).execute();
            boolean createdTableUser = c.prepareStatement(CREATE_TABLE_USER).execute();
            boolean createdTableInterest = c.prepareStatement(CREATE_TABLE_INTEREST).execute();
            created = createdTableUser && createdTableInterest && createdSequenceUser && createdSequenceInterest;
        }catch(SQLSyntaxErrorException e){
            // Se não for um erro por criar uma tabela existente
            if(! e.getLocalizedMessage().contains("ORA-00955")){
                created = false;
                System.out.println("Exception creating table " + e);
            }
        }
        return created;
    }

    /**
     * Gets all items in the table. 
     * 
     * @param whereClause if this parameter is empty, the resulting SQL is
     *      "SELECT * FROM TABLE". If, howeverer, this parameter is "id=___",
     *      the SQL turns to "SELECT * FROM TABLE WHERE id=___".
     */
    private static List<Usuario> getAllUsersWhereClause(String whereClause){
        String sql = "SELECT * FROM UserX order by username";
        if(!whereClause.isEmpty()){
            sql += " where " + whereClause;
        }
        List<Usuario> usuarios = new ArrayList<>();
        try(
            Connection c = getConnection();
            ResultSet res = c.prepareStatement(sql).executeQuery();
        ){
            while(res.next()){
                Usuario u = new Usuario(res.getInt("id"),res.getString("username"),res.getString("password"));
                // Allowed because id is an integer.
                u.setInteresses(getAllInterestsWhereClause("id = " + u.getId()));
                usuarios.add(u);
            }
        }catch(SQLException e){
            System.out.println("Exception!!" + e);
        }
        return usuarios;
    }
    
    private static List<Interesse> getAllInterestsWhereClause(String whereClause){
        String sql = "SELECT * FROM interestX";
        if(!whereClause.isEmpty()){
            sql += " where " + whereClause;
        }
        List<Interesse> interesses = new ArrayList<>();
        try(
            Connection c = getConnection();
            ResultSet res = c.prepareStatement(sql).executeQuery();
        ){
            while(res.next()){
                Interesse i = new Interesse(res.getInt("id"),res.getString("user"));
                // Allowed because id is an integer.
                interesses.add(i);
            }
        }catch(SQLException e){
            System.out.println("Exception!!" + e);
        }
        return interesses;
    }
    
    /**
     * Returns all the Users in the database.
     * @return all the Users in the database.
     */
    public static List<Usuario> getAllUsers(){
        return getAllUsersWhereClause("");
    }
    
    /**
     * Tests if it can log in the database.
     * @return true if succeeded.
     */
    public static boolean testLogin(){
        boolean logsIn = false;
        try(
            Connection c = getConnection();
        ){
            logsIn = c.isValid(0);
        } catch(SQLException e){
            System.out.println("TestLogin Exception " + e);
        }
        return logsIn;
    }
    
   


    public static boolean insertInterest(Interesse interest, int idUser){
        String SQL = "INSERT INTO Interestx(id,value,iduser) VALUES(interest_seq.nextval, ?, ?)"; 
        System.out.println("Inset interest");
        boolean inserted = false;
        
        String columns[] = {"id"};
        try(
            Connection c = getConnection();
            PreparedStatement st = c.prepareStatement(SQL, columns);
        ) {
            st.setString(1, interest.getNome());
            st.setInt(2, idUser);
            st.execute();
            System.out.println("inseriuô");
        } catch(SQLException e){
            System.out.println("Exception " + e);
        }
        return false;
    }
    
    public static boolean insertUser(Usuario u){
        String SQL = "INSERT INTO userx(id,username,password) VALUES(user_seq.nextval, ?, ?)"; 
        boolean inserted = true;
        
        String columns[] = {"id"};
        try(
            Connection c = getConnection();
            PreparedStatement st = c.prepareStatement(SQL, columns);
        ) {
            st.setString(1, u.getEmail());
            st.setString(2, u.getSenha());
            st.execute();
            ResultSet rs = st.getGeneratedKeys();
            
            while(rs.next()){
                u.setId(rs.getInt(1));
            }
         
        } catch(SQLException e){
            System.out.println("Exception " + e);
            inserted = false;
        }
        for(Interesse i : u.getInteresses()){
            System.out.println("Ta tentando inserr");
            inserted &= insertInterest(i, u.getId());
        }
        
        return inserted;
        
    }
    
    
}
