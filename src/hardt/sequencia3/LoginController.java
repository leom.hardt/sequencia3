package hardt.sequencia3;

import hardt.sequencia3.model.Dados;
import hardt.sequencia3.model.UsuarioDB;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class LoginController implements Initializable {

    @FXML
    TextField username;
    @FXML
    PasswordField password;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }    
    
    @FXML
    private void onEntrar(){
        Dados.setPassword(password.getText());
        Dados.setUsername(username.getText());
        if(UsuarioDB.testLogin()){
            try{
                UsuarioDB.createTable();
            } catch(Exception e){
            }

            Main.mudarDeTela("TelaPrincipal.fxml");
        } else {
            
            Dialog<String> d = new Dialog<>();
            d.getDialogPane().setContent(new Label("Senha errada."));
            d.getDialogPane().getButtonTypes().add(ButtonType.OK);
            d.showAndWait();
        }
    }
}
