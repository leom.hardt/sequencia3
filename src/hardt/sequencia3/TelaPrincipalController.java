/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hardt.sequencia3;

import hardt.sequencia3.model.Interesse;
import hardt.sequencia3.model.Usuario;
import hardt.sequencia3.model.UsuarioDB;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 *
 * @author Aluno
 */
public class TelaPrincipalController implements Initializable {
    
    @FXML
    private Label label;
    
    @FXML
    private TextField email, interesses;
    @FXML
    private PasswordField senha, resenha;
    
    @FXML
    private ListView<Usuario> listaView;
    
    
    
    @FXML
    private void onCadastrar(){
        if(senha.getText().equals(resenha.getText())){
            Usuario u = new Usuario(-1, email.getText(), senha.getText());
            System.out.println("Sending value " + email.getText());
            
            ArrayList<Interesse> interessesList = new ArrayList<>();
            String splitInteresses[] = this.interesses.getText().split(",");
            
            for(String inter : splitInteresses) {
                System.out.println("Outra String inter");
                interessesList.add(new Interesse(-1, inter));
            }
            u.setInteresses(interessesList);
            UsuarioDB.insertUser(u);
            atualizarLista();
            
            senha.setText("");
            email.setText("");
            resenha.setText("");
            interesses.setText("");
        } else {
            System.out.println("Senhas diferentes!\n");
        }
      
    }
    
    void atualizarLista(){
        listaView.setItems(FXCollections.observableArrayList());
        List<Usuario> users = UsuarioDB.getAllUsers();
        for(Usuario u : users){
            listaView.getItems().add(u);
        }
    }
    @FXML
    public void onAtualizar(){
        atualizarLista();
    }
    
    @FXML
    void onEditar(){
        Usuario u = listaView.getSelectionModel().getSelectedItem();
        email.setText(u.getEmail());
        interesses.setText(u.getInteressesString());
        senha.setText(u.getSenha());
        resenha.setText(u.getSenha());
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        atualizarLista();
        
    }    
    
}
